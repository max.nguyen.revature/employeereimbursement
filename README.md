# PROJECT NAME

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Java 8
* SQL/JDBC 
* Servlet Technology
* HTML/CSS (with Bootstrap)
* Javascript
* Amazon RDS
* Git

## Features

* Users can login and register
* Users have distinct and meaningful role designations
* Users can view and change their account information
* Employees can submit reimbursement requests
* Employees can view their pending, denied, and accepted requests
* Managers can view all employees and their requests


To-do list:
* Managers should be able to sort the requests in a easier manner
* Managers should be able to delete old requests

## Getting Started

To install:
Navigate to desired local install directory.   
Install with `git clone https://gitlab.com/max.nguyen.revature/employeereimbursement`.
run `mvn install` inside the cloned repository.


## Usage

To start the website:
run the index.html file in the webapp folder.
