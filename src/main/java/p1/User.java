package p1;

public class User {
	
	private String username;
	private String password;
	
	public User() {}
	
	public User(String user, String pass) {
		super();
		this.username = user;
		this.password = pass;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUser(String user) {
		this.username = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String pass) {
		this.password = pass;
	}
	
	

}
