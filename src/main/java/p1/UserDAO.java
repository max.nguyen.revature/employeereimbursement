package p1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserDAO {
	
	private static UserDAO singleton = null;
	
	public static UserDAO getDAO() {
		
		if (singleton == null) {
			singleton = new UserDAO();
		}
		
		return singleton;
	}
	
	public int countUser(String username) throws SQLException {
		
		String checkUser = "SELECT COUNT(username) FROM p1_users WHERE username=?";
		
		PreparedStatement ps = DBcon.getCon().prepareStatement(checkUser);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int count = rs.getInt("COUNT(username)");
		
		return count;
	}
	
	public boolean checkPass(String username, String password) throws SQLException {
		
		String compare = null;
		
		String findPass = "SELECT password FROM p1_users WHERE username=?";
		PreparedStatement ps = DBcon.getCon().prepareStatement(findPass);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			compare = rs.getString("password");
		} else {
			ps.close();
			rs.close();
			return false;
		}
		
		if (compare.equals(password)) {
			ps.close();
			rs.close();
			return true;
		}
		ps.close();
		rs.close();
		return false;
	}
	
	public String getRole(String username) throws SQLException {
		
		String role = "null";
		
		String findRole = "SELECT role FROM p1_users WHERE username=?";
		PreparedStatement ps = DBcon.getCon().prepareStatement(findRole);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			role = rs.getString("role");
		}
				
		ps.close();
		rs.close();
		return role;
	}
	
	public int getEmployeeID(String username) throws SQLException {
		int id = 0;
		
		String findID = "SELECT employeeid FROM p1_employees WHERE username=?";
		PreparedStatement ps = DBcon.getCon().prepareStatement(findID);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			id = rs.getInt("employeeid");
		}
		
		ps.close();
		rs.close();
		
		return id;
	}
	
	public boolean insertUser(String username, String password) throws SQLException {
		
		String insertUser = "INSERT INTO p1_users (username, password, role) VALUES (\'"+username+"\',\'"+password+"\', 'employee')";
		String insertEmployee = "INSERT INTO p1_employees (username, employeeid) VALUES (\'"+username+"\', DEFAULT)";
		Statement stmnt = DBcon.getCon().createStatement();
		stmnt.addBatch(insertUser);
		stmnt.addBatch(insertEmployee);
		stmnt.executeBatch();
		stmnt.close();
		
		return true;
	}
	
	public boolean updatePassword(String username, String password) throws SQLException {
		
		String updatePass = "UPDATE p1_users SET password=? WHERE username=?";
		PreparedStatement ps = DBcon.getCon().prepareStatement(updatePass);
		ps.setString(1, password);
		ps.setString(2, username);
		ps.executeUpdate();
		ps.close();
		
		return true;
	}

	public ArrayList<Employee> getEmployees() throws SQLException {
		ArrayList<Employee> employees = new ArrayList<Employee>();
		
		String getAll = "SELECT * FROM p1_employees";
		PreparedStatement ps = DBcon.getCon().prepareStatement(getAll);
		ResultSet rs = ps.executeQuery();
		
		if (!rs.next()) {
			throw new SQLException();
		} else {
			do {
				Employee r = new Employee(rs.getString("username"), rs.getInt("employeeid") );
				employees.add(r);
			} while (rs.next());
		}	
		
		return employees;
		
	}

}
