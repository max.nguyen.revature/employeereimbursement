package p1;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		
		String jsonstring = request.getReader().readLine();
		ObjectMapper om = new ObjectMapper();
		User u = om.readValue(jsonstring, User.class);
		
		try {
			if (UserDAO.getDAO().countUser(u.getUsername()) > 0) {
				response.getWriter().append("fail");
			} else {
				UserDAO.getDAO().insertUser(u.getUsername(), u.getPassword());
				response.getWriter().append("success");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServletException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new IOException();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
