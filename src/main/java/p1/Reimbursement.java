package p1;

import java.io.Serializable;

public class Reimbursement implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2373719831907148402L;
	private int requestid;
	private int employeeid;
	private float amount;
	private String reason;
	private String status;
	private String description;
	
	public Reimbursement() {}
	
	public Reimbursement(int requestid, int employeeid, float amount, String reason, String status,
			String description) {
		super();
		this.requestid = requestid;
		this.employeeid = employeeid;
		this.amount = amount;
		this.reason = reason;
		this.status = status;
		this.description = description;
	}
	
	public int getRequestid() {
		return requestid;
	}
	public void setRequestid(int requestid) {
		this.requestid = requestid;
	}
	public int getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
