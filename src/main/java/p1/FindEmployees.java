package p1;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * Servlet implementation class FindEmployees
 */
public class FindEmployees extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindEmployees() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		String username = (String) request.getSession().getAttribute("username");
		if (username == null) {
			throw new ServletException();
		}
		
		try {
			ArrayList<Employee> employees = UserDAO.getDAO().getEmployees();
			ObjectMapper om = new ObjectMapper();
			ArrayNode jsonarray = om.createArrayNode();
			
			for (Employee emp: employees) {
				jsonarray.addPOJO(emp);
			}
			
			response.getWriter().write(om.writeValueAsString(jsonarray));
			
		} catch (Exception e) {
			throw new ServletException();
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
