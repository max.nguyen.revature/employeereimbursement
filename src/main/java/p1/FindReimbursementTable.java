package p1;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * Servlet implementation class FindReimbursementTable
 */
public class FindReimbursementTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindReimbursementTable() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		String username = (String) request.getSession().getAttribute("username");
		if (username == null) {
			throw new ServletException();
		}
		int id;
		String role;
		try {
			id = UserDAO.getDAO().getEmployeeID(username);
			role = UserDAO.getDAO().getRole(username);
			
			ArrayList<Reimbursement> reimbursements;
			if (role.equals("manager")) {
				reimbursements = ReimbursementDAO.getDAO().getReimbursements();
			} else {
				reimbursements = ReimbursementDAO.getDAO().getReimbursements(id);
			}
			
			ObjectMapper om = new ObjectMapper();
			ArrayNode jsonarray = om.createArrayNode();
			
			for (Reimbursement item: reimbursements) {
//				ObjectNode node = om.createObjectNode();
//				node.put("requestid", item.getRequestid());
//				node.put("employeeid", item.getEmployeeid());
//				node.put("amount", item.getAmount());
//				node.put("reason", item.getReason());
//				node.put("status", item.getStatus());
//				node.put("description", item.getDescription());
//				jsonarray.add(node);
				jsonarray.addPOJO(item);
			}
			
			response.getWriter().write(om.writeValueAsString(jsonarray));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServletException();
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
