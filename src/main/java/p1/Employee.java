package p1;

public class Employee {
	
	private String username;
	private int employeeid;
	
	public Employee() {}
	
	public Employee(String username, int employeeid) {
		super();
		this.username = username;
		this.employeeid = employeeid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}
	

}
