package p1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		
		String jsonstring = request.getReader().readLine();
		ObjectMapper om = new ObjectMapper();
		User u = om.readValue(jsonstring, User.class);
//		System.out.println(u.getUsername());
//		System.out.println(u.getPassword());
		
		if (Verification.userVerified(u)) {
			request.getSession().setAttribute("username", u.getUsername());
			response.getWriter().append("verified");
		} else {
			request.getSession().invalidate();
			response.getWriter().append("failed");
		}
//		if (u.getUsername().equals("max")) {
//			request.getSession().setAttribute("username", u.getUsername());
//			response.getWriter().append("verified");
//		} else {
//			request.getSession().invalidate();
//			response.getWriter().append("failed");
//		}
		
		
//		response.sendRedirect("dash.html");
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
