package p1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ReimbursementDAO {
	
	private static ReimbursementDAO singleton = null;
	
	public static ReimbursementDAO getDAO() {
		
		if (singleton == null) {
			singleton = new ReimbursementDAO();
		}
		
		return singleton;
	}
	
	public ArrayList<Reimbursement> getReimbursements(int id) throws SQLException {
		ArrayList<Reimbursement> reimbursements = new ArrayList<Reimbursement>();
		
		String getAll = "SELECT * FROM p1_reimbursements WHERE employeeid=?";
		
		PreparedStatement ps = DBcon.getCon().prepareStatement(getAll);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		if (!rs.next()) {
			throw new SQLException();
		} else {
			do {
				Reimbursement r = new Reimbursement(rs.getInt("requestid"), rs.getInt("employeeid"), rs.getFloat("amount"), rs.getString("reason"), rs.getString("status"), rs.getString("description") );
				reimbursements.add(r);
			} while (rs.next());
		}				
		
		rs.close();
		ps.close();
		return reimbursements;
		
	}
	
	public ArrayList<Reimbursement> getReimbursements() throws SQLException {
		ArrayList<Reimbursement> reimbursements = new ArrayList<Reimbursement>();
		
		String getAll = "SELECT * FROM p1_reimbursements";
		
		PreparedStatement ps = DBcon.getCon().prepareStatement(getAll);
		ResultSet rs = ps.executeQuery();
		
		if (!rs.next()) {
			throw new SQLException();
		} else {
			do {
				Reimbursement r = new Reimbursement(rs.getInt("requestid"), rs.getInt("employeeid"), rs.getFloat("amount"), rs.getString("reason"), rs.getString("status"), rs.getString("description") );
				reimbursements.add(r);
			} while (rs.next());
		}				
		
		rs.close();
		ps.close();
		return reimbursements;
		
	}
	
	public boolean addReimbursement(Reimbursement req) throws SQLException {
		
		String insertString = "INSERT INTO p1_reimbursements (requestid, employeeid, amount, reason, description, status) VALUES (DEFAULT, ?, ?, ?, ?, ?)";
		PreparedStatement ps = DBcon.getCon().prepareStatement(insertString);
		ps.setInt(1, req.getEmployeeid());
		ps.setFloat(2, req.getAmount());
		ps.setString(3, req.getReason());
		ps.setString(4, req.getDescription());
		ps.setString(5, req.getStatus());
		ps.executeUpdate();
		ps.close();
		
		return true;
	}
	
	public void approveReimbursement(int id) throws SQLException {
		
		String approveString = "UPDATE p1_reimbursements SET status=? WHERE requestid=?";
		PreparedStatement ps = DBcon.getCon().prepareStatement(approveString);
		ps.setString(1, "APPROVED");
		ps.setInt(2, id);
		ps.executeUpdate();
		ps.close();
		
	}
	
	public void denyReimbursement(int id) throws SQLException {
		
		String denyString = "UPDATE p1_reimbursements SET status=? WHERE requestid=?";
		PreparedStatement ps = DBcon.getCon().prepareStatement(denyString);
		ps.setString(1, "DENIED");
		ps.setInt(2, id);
		ps.executeUpdate();
		ps.close();
		
	}

}
