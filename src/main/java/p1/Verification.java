package p1;

import java.sql.SQLException;

public class Verification {
	
	public static boolean userVerified(User u) {
		
		try {
			if (UserDAO.getDAO().countUser(u.getUsername())!=1) {
				//returns false if username does not exist
				return false;
			}
			if (UserDAO.getDAO().checkPass(u.getUsername(), u.getPassword())) {
				//returns true if password matches username
//				System.out.println("password matches");
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//returns false by default
		return false;
	}

}
