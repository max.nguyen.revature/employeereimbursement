let give_em_the_boot = async function(){

    let response = await fetch("/p1/CheckLogin", {method: "POST" , credentials: "include"}).catch((error) => console.log("error authenticating"));

    let result = await response.text();

        // console.log(result);

    if (result === "new") {
        window.location.href = "/p1";
    }
}

let logout = async function(){

    let response = await fetch("/p1/Logout", {method: "POST", credentials: "include"}).catch((error) => console.log("error logging out"));

    let result = await response.text();

    if ( result === "success") {
        window.location.href = "/p1";
    } else {
        alert("Error logging out, please try again!")
    }
}