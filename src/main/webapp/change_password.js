let sendPass = async function() {

    let payload = {
        password : document.getElementById("form_pass").value
    }

    let response = await fetch("/p1/ChangePassword", {method:"POST", headers:{"Content-Type":"application/json"} , credentials:"include", body: JSON.stringify(payload) }).catch((error) => {alert("Fetch failed.")});

    let result = await response.text();

    if (result == "success") {
        alert("Password changed successfully");
        window.location.href = "/p1/dash.html";
    } else {
        alert("Error setting new password, try again.")
    }

}