let fetch_error = function(){
	let error_field = document.getElementById("error_field");
	error_field.innerHTML = "<p style=\"color:red\"> Error communicating with server <p>";
}

let verify_login = async function(){
	let error_field = document.getElementById("error_field");
	error_field.innerHTML = "";

	var u = document.getElementById("user").value;
	var p = document.getElementById("pass").value;

	let credentials = {
		username : u,
		password : p
	};
	
	// console.log(credentials.username);
	// console.log(credentials.password);
	
	let response = await fetch("/p1/Login", {method:"POST", headers: {"Content-Type": "application/json"}, credentials: 'include', body: JSON.stringify(credentials)}).catch((error) => fetch_error());

	
	let result = await response.text();
	if (result==='verified'){
		window.location.href = 'dash.html';
	} else {
		error_field.innerHTML = "<p style=\"color:red\"> Username or password is incorrect <p>"
	}
	//console.log(result);

	//console.log("made it here");
	//console.log(result);
	
};

let register = async function(){
	let error_field = document.getElementById("error_field");
	error_field.innerHTML = "";

	var u = document.getElementById("user").value;
	var p = document.getElementById("pass").value;

	let credentials = {
		username : u,
		password : p
	};

	let response = await fetch("/p1/Register", {method:"POST", headers: {"Content-Type": "application/json"}, credentials: 'include', body: JSON.stringify(credentials)}).catch((error) => fetch_error());
	
	let result = await response.text();
	
	if (result=='success'){
		error_field.innerHTML = "<p style=\"color:green\"> Registered successfully, please log in! <p>";
	} else {
		error_field.innerHTML = "<p style=\"color:red\"> That username already exists! <p>"
	}
}