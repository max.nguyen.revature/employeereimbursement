$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
    });
    $('.side-nav .collapse').on("show.bs.collapse", function() {                        
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
    });
})

let showEmployees = async function(){

    role = await getRole();
    if (role != "manager") {
        return;
    }

    var head = document.getElementById("emphead");
    var body = document.getElementById("empbody");

    let employees = await getEmployees();

    var header = document.createElement("th");
    header.innerHTML = "Username";
    head.appendChild(header);

    var header = document.createElement("th");
    header.innerHTML = "Employee ID";
    head.appendChild(header);



    for (person of employees) {
        var nr = document.createElement('tr');

        var cell = document.createElement('td');
        cell.appendChild(document.createTextNode(person.username));
        nr.appendChild(cell);

        var cell = document.createElement('td');
        cell.appendChild(document.createTextNode(person.employeeid));
        nr.appendChild(cell);

        body.appendChild(nr);


    }

}
    
let getEmployees = async function(){

    let response = await fetch("/p1/FindEmployees", {method:"GET", headers:{"Accept":"application/json"}, credentials:"include"}).catch((error) => {alert("Fetch failed.")});

    let employees = await response.json();

    console.log(employees);

    return employees;
}

let buildTable = async function(){

    let userRole = await getRole();
    
    //getting the table head
    var head = document.getElementById("tablehead");

    while (head.firstChild) {
        head.removeChild(head.firstChild);
    }

    if (userRole == "employee"){

        //set employee headers
        //req id header
        var reqid = document.createElement("th");
        reqid.innerHTML = "Request ID";
        head.appendChild(reqid);

        //reason header
        var rhead = document.createElement("th");
        rhead.innerHTML = "Reason";
        head.appendChild(rhead);

        var amnt = document.createElement("th");
        amnt.innerHTML = "Amount";
        head.appendChild(amnt);

        var desc = document.createElement("th");
        desc.innerHTML = "Description";
        head.appendChild(desc);

        var stat = document.createElement("th");
        stat.innerHTML = "Status";
        head.appendChild(stat);

        let reimbursements = await getReimbursements();

        //get table body
        var body = document.getElementById("tablebody");
        //clears body
        while (body.firstChild) {
            body.removeChild(body.firstChild);
        } 

        //puttting all data in table
        for (item of reimbursements){
            var nr = document.createElement('tr');

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.requestid));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.reason));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode("$"+item.amount));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.description));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.status));
            nr.appendChild(cell);

            body.appendChild(nr);
        }

    } else if ( userRole == "manager") {

        var reqid = document.createElement("th");
        reqid.innerHTML = "Request ID";
        head.appendChild(reqid);

        var empid = document.createElement("th");
        empid.innerHTML = "Employee ID";
        head.appendChild(empid);

        var rhead = document.createElement("th");
        rhead.innerHTML = "Reason";
        head.appendChild(rhead);

        var amnt = document.createElement("th");
        amnt.innerHTML = "Amount";
        head.appendChild(amnt);

        var desc = document.createElement("th");
        desc.innerHTML = "Description";
        head.appendChild(desc);

        var stat = document.createElement("th");
        stat.innerHTML = "Status";
        head.appendChild(stat);

        var actionhead = document.createElement("th");
        actionhead.innerHTML = "Action";
        head.appendChild(actionhead);

        let reimbursements = await getReimbursements();

        var body = document.getElementById("tablebody");
        while (body.firstChild) {
            body.removeChild(body.firstChild);
        }

        for (item of reimbursements){
            var nr = document.createElement('tr');

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.requestid));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.employeeid));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.reason));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode("$"+item.amount));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.description));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            cell.appendChild(document.createTextNode(item.status));
            nr.appendChild(cell);

            var cell = document.createElement('td');
            let approveButton = document.createElement("div");
            approveButton.innerHTML = "<input type=\"button\" onclick=\"approve("+item.requestid+")\" value=\"APPROVE\"> ";
            let denyButton = document.createElement("div");
            denyButton.innerHTML = "<input type=\"button\" onclick=\"deny("+item.requestid+")\" value=\"DENY\">";
            cell.appendChild(approveButton);
            cell.appendChild(denyButton);
            nr.appendChild(cell);

            

            body.appendChild(nr);
        }
      
    } else {
        console.log("no role here");
    }

}

let getRole = async function(){

    let url = "/p1/FindRole";

    let response = await fetch(url, {method: 'POST', credentials:"include"}).catch((error) => {alert("error")})

    let role = await response.text();

    // console.log(role);

    return role;


}

let getReimbursements = async function(){

    let url = "/p1/FindReimbursementTable";

    let response = await fetch(url, {headers: {"Accept" : "application/json"}, method: 'POST', credentials:'include'}).catch((error) => {alert("error")})

    let result = await response.json();

    // console.log(result);

    return result;
}

let approve = async function(requestid) {

    let response = await fetch("/p1/Approve", {method: "POST", credentials:"include", body: requestid}).catch((error) => {alert("error")});

    let result = await response.text();

    if (result == "success"){
        await buildTable();
    }
}

let deny = async function(requestid) {

    let response = await fetch("/p1/Deny", {method: "POST", credentials:"include", body: requestid}).catch((error) => {alert("error")});

    let result = await response.text();

    if (result == "success"){
        await buildTable();
    }
}