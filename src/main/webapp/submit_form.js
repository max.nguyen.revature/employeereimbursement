let submit_req = async function(){

    let newreq = {
        requestid : null,
        employeeid : null,
        amount : document.getElementById("form_amount").value,
        reason : document.getElementById("form_reason").value,
        description : document.getElementById("form_description").value,
        status : "PENDING"
    };

    let response = await fetch("/p1/NewRequest", { method : "POST", headers: {"Content-Type": "application/json"}, credentials:"include", body: JSON.stringify(newreq)}).catch((error) => {alert("Error with request")});

    let result = await response.text();

    if (result == "success") {
        alert("Request submitted successfully!")
        window.location.href = "dash.html"
    } else {
        alert("Request submission failed.")
    }
}